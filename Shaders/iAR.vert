attribute vec3 Position;
attribute vec3 Normal;
attribute vec4 SourceColor;

varying vec4 DestinationColor;

uniform mat4 Projection;
uniform mat4 ModelView;
uniform mat3 Rotation;

vec4 LightPosition = vec4(5.0, 5.0, -5.0, 1.0);

void main(void) {
	vec4 Vertex = vec4(Position.x, Position.y, Position.z, 1.0);

	vec3 EyeNormal = normalize(Rotation * Normal);

	vec3 LightDirection =
		normalize((Projection * LightPosition).xyz - (ModelView * Vertex).xyz);

	float Difference = max(0.0, dot(EyeNormal, LightDirection));

	DestinationColor.rgb = Difference * SourceColor.rgb;
	DestinationColor.a = SourceColor.a;

	gl_Position = (Projection * ModelView) * Vertex;
}
