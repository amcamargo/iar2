/**
 * @file
 * @author  Jonathan Hess (2011/2) <jhess666@gmail.com>
 * @author  Allan Camargo (2013/1)
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * App view controller class.
 */

#import <UIKit/UIKit.h>

#import <vector>

#import "Types.h"
#import "VideoInput.h"

@interface iARViewController : UIViewController {
	RuntimeInfo info;

#if TARGET_OS_EMBEDDED
	VideoInput *videoInput;
#endif

	// aruco::CameraParameters cameraParams;

	CGPoint firstTouch;
}

@property (nonatomic, retain) UIView *previewer;
@property (nonatomic, retain) UIView *interface;
@property (nonatomic, strong) UIImage *currFrame;

/**
 * Configure all workspace data.
 */
- (void)setup;

/**
 * Call custom modifications routines.
 */
- (void)customize;

/**
 * Refresh all data inside custom views.
 */
- (void)refresh;

/**
 * Device orientation changed callback.
 */
- (void)didDeviceOrientationChange:(NSNotification *)notification;

/**
 * Capture buffer processing finished callback.
 */
- (void)didCaptureBufferProcessingFinish:(NSNotification *)notification;

@end
