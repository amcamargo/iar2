/**
 * @file
 * @author  Jonathan Hess <jhess666@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Video input class.
 */

#import "VideoInput.h"

#if TARGET_OS_EMBEDDED
@implementation VideoInput

@synthesize videoOutput;
@synthesize buffer;

- (void)dealloc {
	[session release];

	[super dealloc];
}

// Code adapted from "Augmented reality on the iPhone with iOS4.0" (20th Sep. 2011)
// http://cmgresearch.blogspot.com/2010/10/augmented-reality-on-iphone-with-ios40.html

- (id)initWithFrame:(CGRect)frame andFillInfo:(RuntimeInfo &)info_ {
	self = [super init];
	if (self) {
		session = [[AVCaptureSession alloc] init];
		[session setSessionPreset:AVCaptureSessionPresetMedium];

		info_.inputSz.width = 480;
		info_.inputSz.height = 360;

		AVCaptureDevice *camera = [AVCaptureDevice
								   defaultDeviceWithMediaType:AVMediaTypeVideo];

		NSError *err = nil;
		AVCaptureInput *cameraInput = [AVCaptureDeviceInput
									   deviceInputWithDevice:camera
									   error:&err];

		videoOutput = [[AVCaptureVideoDataOutput alloc] init];
		videoOutput.alwaysDiscardsLateVideoFrames = YES;

		videoOutput.minFrameDuration = CMTimeMake(1, 30);

		dispatch_queue_t captureQueue = dispatch_queue_create("captureQueue", NULL);
		[videoOutput setSampleBufferDelegate:self queue:captureQueue];
		dispatch_release(captureQueue);

		videoOutput.videoSettings = [NSDictionary dictionaryWithObjectsAndKeys:
									 [NSNumber numberWithUnsignedInt:kCVPixelFormatType_32BGRA],
									 (id)kCVPixelBufferPixelFormatTypeKey,
									 nil];

		if (![session canAddInput:cameraInput]) {
			[NSException raise:@"Camera input error"
						format:@"It's impossible to add a camera input"];
		}
		[session addInput:cameraInput];

		if (![session canAddOutput:videoOutput]) {
			[NSException raise:@"Video output error"
						format:@"It's impossible to add a video output"];
		}
		[session addOutput:videoOutput];

		info = &info_;
	}
	return self;
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
	if (sampleBuffer) {
		@synchronized(self) {
			cv::Rect roi((info->inputSz.width - info->viewportSz.height) / 2,
						 (info->inputSz.height - info->viewportSz.width) / 2,
						 info->viewportSz.height,
						 info->viewportSz.width);

			cv::Mat tempImg = [Utils createMatFromSampleBuffer:sampleBuffer withROI:roi];
			cv::flip(tempImg, buffer, 0);

			cv::Mat img(cvSize(info->viewportSz.width, info->viewportSz.height), CV_8UC4);
			cv::transpose(buffer, img);

			buffer = img;
		}

		[[NSNotificationCenter defaultCenter] postNotificationName:CaptureBufferProcessingDidFinishNotification
															object:self];
	}
	else {
		NSLog(@"No sample buffer, ignoring...");
	}
}

- (void)startCapturing {
	[session startRunning];
}

- (void)stopCapturing {
	[session stopRunning];
}

@end
#endif
