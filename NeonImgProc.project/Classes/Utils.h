/**
 * @file
 * @author  Jonathan Hess <jhess666@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Objective-C++ utils.
 */

#import <AVFoundation/AVFoundation.h>
#import <string>
#import <opencv2/opencv.hpp>

@interface Utils : NSObject {

}

/**
 * Get string path to a resource file.
 *
 * @param filename The filename
 * @param ext The file extension
 * @returns The string object
 */
+ (std::string)getStrResourcePathForFile:(NSString *)filename ofType:(NSString *)ext;

/**
 * Create Mat from a sample buffer with defined size.
 *
 * @param sampleBuffer The sample buffer object
 * @param roi The Rect bounds size
 * @return The Mat object
 */
+ (cv::Mat)createMatFromSampleBuffer:(CMSampleBufferRef)sampleBuffer withROI:(cv::Rect)roi;

/**
 * Create Mat from an UIImage.
 *
 * @param image The image
 * @return The Mat object
 */
+ (cv::Mat)cvMatWithImage:(UIImage *)image;

/**
 * Create UIImage from a sample buffer.
 *
 * @param sampleBuffer The sample buffer object
 * @return The UIImage object
 */
+ (UIImage *)createUIImageFromSampleBuffer:(CMSampleBufferRef)sampleBuffer;

/**
 * Create UIImage from a Mat.
 *
 * @param img The Mat object
 * @return The UIImage object
 */
+ (UIImage *)createUIImageFromMat:(cv::Mat)cvMat;

/**
 * Save UIImage to the photos album.
 *
 * @param img The UIImage object
 */
+ (void)saveUIImage:(UIImage *)img;

@end
