/**
 * @file
 * @author  Jonathan Hess <jhess666@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Object manager singleton class.
 */

#import <map>

#import "Object.h"

class ObjectManager {
private:
	static bool instanceFlag;
	static ObjectManager *single;

	/**
	 * Construct unique instance of object.
	 */
	ObjectManager();

public:
	std::map<int, Object *> objects;

	/**
	 * Destruct unique instance of object.
	 */
	~ObjectManager() {
		instanceFlag = false;

		std::map<int, Object *>::iterator o = objects.begin();
		while (o != objects.end()) {
			delete o->second;
			o++;
		}
		objects.clear();
	}

	/**
	 * Get unique ObjectManager instance.
	 *
	 * @returns The ObjectManager object
	 */
	static ObjectManager * getInstance();

	/**
	 * Create Object and link it with given ID.
	 *
	 * @param id_ The integer ID from 0 to 1023
	 * @returns The Object object
	 */
	Object * addObject(int id_);

	/**
	 * Get Object for a given ID.
	 *
	 * @param id_ The integer ID from 0 to 1023
	 * @returns The Object object, or NULL
	 */
	Object * objectWithID(int id_);
};
