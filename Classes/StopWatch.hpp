/**
 * @file StopWatch.hpp
 * @author Allan Camargo
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Created for time parts of code, creates a list based on times points (Laps, Keyframes, whatever...) 
 *
 * Win32 and OSX only!
 */

#ifndef _StopWatch_hpp
#define _StopWatch_hpp

#if defined WIN32 || defined _WIN32 || defined WINCE
	#include <windows.h>
#else
	#include <mach/mach_time.h>
#endif

#include <vector>

class StopWatch
{
    long startTime;
    std::vector<std::pair<double, const char*> > marks;

    long getTickCount()
    {
#if defined WIN32 || defined _WIN32 || defined WINCE
		LARGE_INTEGER counter;
		QueryPerformanceCounter(&counter);
		return (int64)counter.QuadPart;
#else
		return (int64)mach_absolute_time();
#endif
    }
    
    double getTickFrequency()
    {
        static double freq = 0;

        if( freq == 0 )
        {
#if defined WIN32 || defined _WIN32 || defined WINCE
        	LARGE_INTEGER lInt;
	        QueryPerformanceFrequency(&lInt);
			freq = lInt.QuadPart;
#else
            mach_timebase_info_data_t sTimebaseInfo;
            mach_timebase_info(&sTimebaseInfo);
            freq = sTimebaseInfo.denom*1e9/sTimebaseInfo.numer;
#endif
        }
        return freq;
    }
    
public:
    
    double value;
    
    StopWatch()
    {
        reset();
    }
    
    ~StopWatch()
    {
        marks.clear();
    }

    void start()
    {
        startTime = getTickCount();
    }
    
    void mark(const char* name)
    {
    	this->stop();
        marks.push_back(std::pair<double, const char*>(this->value * 1000.0, name));
        value = 0;
        this->start();
    }
    
    void stop()
    {
        long stopTime = getTickCount();
        value += ((double)stopTime - startTime) / getTickFrequency();
    }
    
    void reset()
    {
        marks.clear();
        value = 0;
    }
    
    void outputMarkers() const
    {
        for (int i = 0; i < marks.size(); ++i)
        {
            std::cout << marks[i].second << " - " << marks[i].first << std::endl;
        }
    }
    
    std::vector<std::pair<double, const char*> >& getMarks()
    {
        return marks;
    }
};

#endif
