/**
 * @file
 * @author  Jonathan Hess <jhess666@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Object manager singleton class.
 */

#import "ObjectManager.h"

// Code adapted from "Singleton Pattern & its implementation with C++" (25th Sep. 2011)
// http://www.codeproject.com/KB/cpp/singletonrvs.aspx

bool ObjectManager::instanceFlag = false;

ObjectManager * ObjectManager::single = NULL;

ObjectManager * ObjectManager::getInstance() {
	if (!instanceFlag) {
		single = new ObjectManager();
		instanceFlag = true;
	}
	return single;
}

ObjectManager::ObjectManager() {
}

Object * ObjectManager::addObject(int id_) {
	Object *obj = new Object();
	objects[id_] = obj;
	return obj;
}

Object * ObjectManager::objectWithID(int id_) {
	std::map<int, Object *>::iterator o = objects.find(id_);
	if (o != objects.end()) {
		return o->second;
	}
	return NULL;
}
