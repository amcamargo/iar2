/**
 * @file
 * @author  Jonathan Hess (2011/2) <jhess666@gmail.com>
 * @author  Allan Camargo (2013/1)
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * General project types.
 */

#import <GLTools.h>

/**
 * Represents 2-dimensional sizes.
 */
typedef struct {
	int width;
	int height;
} Sz;

/**
 * Represents flags used by general routines.
 */
typedef struct {
	bool detailing;
	bool logging;
    bool benchMark;
} Flags;

/**
 * Represents info data used by general routines.
 */
typedef struct {
	Sz inputSz;
	Sz viewportSz;
	float markerSz;
	M3DMatrix44f projection;

	Flags flags;
} RuntimeInfo;
